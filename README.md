# Requirements

This tool requires the python3 intrepreter. You can find it here:
https://www.python.org/downloads/

# Installation
Download the code of this repository and run this command inside the folder
```
pip3 install .
```

# Usage

Execute the command

```
fillwikipedia
```

This will ask you your username and osm password to be able to edit osm as you. 
After this it will ask you the bounding box of the edit zone in the following format
```
(South,West,North,East)
```
And finally the default language code of the zone
